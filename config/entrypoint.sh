#!/bin/sh
GAME_DIR=/home/steam/Steam/steamapps/common/VRisingDedicatedServer
SETTINGS_DIR=$GAME_DIR/VRisingServer_Data/StreamingAssets/Settings
check_variable_settings() {
if [ -z "${V_RISING_NAME}" ]; then
    echo "V_RISING_NAME has to be set"

    exit
else
    echo "V_RISING_NAME is ${V_RISING_NAME}"    
fi
}

if [ ! -f "/settings/ServerGameSettings.json" ]; then
  check_variable_settings
fi

./steamcmd.sh +@sSteamCmdForcePlatformType windows +login anonymous +app_update validate 1829350 +quit
cp /settings $SETTINGS_DIR
cp -r /mods/* $GAME_DIR

cd $GAME_DIR
xvfb-run -a wine ./VRisingServer.exe -persistentDataPath Z:\\saves