#!/bin/bash

apt update
apt install -y docker docker-compose
sudo usermod -aG docker $USER
newgrp docker
docker run hello-world